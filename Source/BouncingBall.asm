[DEFAULT REL]
[BITS 64]

%include './CSFML/SfEventsH.asm'
%include './CSFML/SfKeysH.asm'
%include './CSFML/SfVideoH.asm'
%include './CSFML/SfColorsH.asm'
%include './CSFML/SfWindowH.asm'
%include './CSFML/SfShapesH.asm'
%include './LibC/Memory.asm'
%include './PodNet/CSystemH.asm'

global main


segment .data

lpwndMainWindow:                dq      0
lpszWindowName:                 db      "AsmPong By Pod",0x00 
lpevtWindowEvent:               dq      0

lpshpBall:                      dq      0
fBallSize:                    	dd      25.0
fBallPos:			dd	300.0,400.0
dwBallMoveRight:		dd	1
dwBallMoveUp:			dd 	1
fBallUpdate:			dd	10.0,-5.0
fBallUpdateMultiplier:		dd	1.0,1.0

fPosTwoX:			dd	2.0,0.0
fNegTwoX:			dd	-2.0,0.0
fPosTwoY:			dd	0.0,2.0
fNegTwoY:			dd	0.0,-2.0


lpshpPaddleLeft:		dq 	0
lpshpPaddleRight:		dq 	0


fPaddleLeftPos:			dd	10.0,450.0
fPaddleRightPos:		dd 	900.0,450.0
;fPaddleSize:			dd	10.0,200.0
fMoveUp:			dd	0,-50.0
fMoveDown:			dd	0,50.0

;videomode:      ISTRUC sfVideoMode
;AT sfVideoMode.Width,           dd      900
;AT sfVideoMode.Height,          dd      975
;AT sfVideoMode.BitsPerPixel,    dd      32
;IEND

; This is for ball only mode, full screen for i3 @ 1920x1080
fPaddleSize 			dd 	0.0,0.0
videomode:      ISTRUC sfVideoMode
AT sfVideoMode.Width,           dd      1835
AT sfVideoMode.Height,          dd      975
AT sfVideoMode.BitsPerPixel,    dd      32
IEND


segment .text


main:
        push rbp
        mov rbp, rsp
     
        mov rdi, 24
        sub rsp, 8
        call malloc wrt ..plt
        add rsp, 8
        mov qword [lpevtWindowEvent], rax

        ; Create a circle
        call sfCircleShape_create wrt ..plt
        mov qword [lpshpBall], rax
        mov rbx, rax
        mov rdi, rax
        movss xmm0, dword [fBallSize]
        sub rsp, 16
        call sfCircleShape_setRadius wrt ..plt
        add rsp, 16
        mov rdi, rbx
        mov rsi, [sfWhite]
        sub rsp, 16
        call sfCircleShape_setFillColor wrt ..plt
        add rsp, 16

	; Create Paddle 1
	call sfRectangleShape_create wrt ..plt
	mov qword [lpshpPaddleLeft], rax
	mov rbx, rax
	movq xmm0, qword [fPaddleSize]
	mov rdi, rbx 
	sub rsp, 16
	call sfRectangleShape_setSize wrt ..plt
	add rsp, 16
	mov rdi, rbx
	mov rsi, [sfWhite]
	sub rsp, 16
	call sfRectangleShape_setFillColor wrt ..plt
	add rsp, 16

	; Create Paddle 2
	call sfRectangleShape_create wrt ..plt
	mov qword [lpshpPaddleRight], rax
	mov rbx, rax
	movq xmm0, qword [fPaddleSize]
	mov rdi, rbx
	sub rsp, 16
	call sfRectangleShape_setSize wrt ..plt
	add rsp, 16
	mov rdi, rbx
	mov rsi, [sfWhite]
	sub rsp, 16
	call sfRectangleShape_setFillColor wrt ..plt
	add rsp, 16


	; Set Ball Position
	mov rdi, [lpshpBall]
	movq xmm0, qword [fBallPos]
	sub rsp, 16
	call sfCircleShape_setPosition wrt ..plt
	add rsp, 16

	; Set Left Paddle Initial Position
	mov rdi, [lpshpPaddleLeft]
	movq xmm0, qword [fPaddleLeftPos]
	sub rsp, 16
	call sfRectangleShape_setPosition wrt ..plt
	add rsp, 16

	; Set Right Paddle Initial Position
	mov rdi, [lpshpPaddleRight]
	movq xmm0, qword [fPaddleRightPos]
	sub rsp, 16
	call sfRectangleShape_setPosition wrt ..plt
	add rsp, 16


        mov rdi, [videomode]
	; Since the struc is on the stack, and is larger than 8 bytes, do magic.
        mov esi, [videomode + sfVideoMode.BitsPerPixel] 
        lea rdx, [lpszWindowName]
        mov rcx, 0x07
        xor r8, r8
        sub rsp, 32
        call sfRenderWindow_create wrt ..plt
        add rsp, 32
        mov qword [lpwndMainWindow], rax

        __WindowLoop:
        mov rdi, qword [lpwndMainWindow]
        sub rsp, 8
        call sfRenderWindow_isOpen wrt ..plt
        add rsp, 8
        cmp rax, 0
        je __WindowLoopBreak

        __EventLoop:
        mov rdi, qword [lpwndMainWindow]
        mov rsi, qword [lpevtWindowEvent]
        sub rsp, 16
        call sfRenderWindow_pollEvent wrt ..plt
        add rsp, 16
        cmp rax, 0
        je __EventLoopBreak        


        mov rax, qword [lpevtWindowEvent + sfKeyEvent.Type]
        mov ecx, dword [rax]
        cmp ecx, sfEvtKeyPressed
        jne __EventLoopBreak

        mov rax, [lpevtWindowEvent]
        add rax, sfKeyEvent.KeyCode
        mov ecx, dword [rax]
        cmp ecx, sfKeyEscape
        je __CallCloseWindow
        cmp ecx, sfKeyQ
        je __CallCloseWindow
 	cmp ecx, sfKeyW
	je __CallMoveLeftPaddleUp
	cmp ecx, sfKeyS
	je __CallMoveLeftPaddleDown
	cmp ecx, sfKeyUp
	je __CallMoveRightPaddleUp
	cmp ecx, sfKeyDown 
	je __CallMoveRightPaddleDown
	jmp __EventLoopBreak

	__CallMoveLeftPaddleUp:
	call __MoveLeftPaddleUp
	jmp __EventLoopBreak

	__CallMoveLeftPaddleDown:
	call __MoveLeftPaddleDown
	jmp __EventLoopBreak

	__CallMoveRightPaddleUp:
	call __MoveRightPaddleUp
	jmp __EventLoopBreak
	
	__CallMoveRightPaddleDown:
	call __MoveRightPaddleDown
	jmp __EventLoopBreak

        __CallCloseWindow:
        call __CloseWindow
        
        __EventLoopBreak:    

        ; More window stuff
        mov rbx, [lpwndMainWindow]
	mov rdi, rbx 
        mov rsi, [sfBlack]
        sub rsp, 16
        call sfRenderWindow_clear wrt ..plt
        add rsp, 16

	; Draw Ball
	call __BallUpdate
        mov rdi, rbx 
        mov rsi, [lpshpBall],
        xor rdx, rdx
        sub rsp, 32
        call sfRenderWindow_drawCircleShape wrt ..plt
        add rsp, 32

	; Draw Left Paddle
	mov rdi, rbx 
	mov rsi, [lpshpPaddleLeft]
	xor rdx, rdx
	sub rsp, 32
	call sfRenderWindow_drawRectangleShape wrt ..plt
	add rsp, 32

	; Draw Right Paddle
	mov rdi, rbx
	mov rsi, [lpshpPaddleRight]
	xor rdx, rdx
	sub rsp, 32
	call sfRenderWindow_drawRectangleShape wrt ..plt
	add rsp, 32

	; Render the window
        mov rdi, rbx
        sub rsp, 16
        call sfRenderWindow_display wrt ..plt
        add rsp, 16

	; Force ~100 fps
	mov rdi, 10000000
	sub rsp, 16
	call NanoSleep wrt ..plt
	add rsp, 16

        jmp __WindowLoop
        __WindowLoopBreak:

	mov rdi, [lpevtWindowEvent]
	sub rsp, 16
	call free wrt ..plt
	add rsp, 16
	
	mov rsp, rbp
	pop rbp

        ret





__CloseWindow:
        mov rdi, qword [lpwndMainWindow]
        sub rsp, 8
        call sfRenderWindow_close wrt ..plt
        add rsp, 8
        ret


__BallUpdate:
	call __MoveBallLogic
	movq xmm1, qword [fBallUpdateMultiplier]
	mov rdi, [lpshpBall]
	mov rcx, qword [fBallUpdate]
	movq xmm0, rcx
	mulps xmm0, xmm1
	sub rsp, 16
	call sfCircleShape_move wrt ..plt
	add rsp, 16
	ret


__MoveBallLogic:
	mov rdi, [lpshpBall]
	
	__MoveBallGetPosition:
	sub rsp, 16
	call sfCircleShape_getPosition wrt ..plt
	add rsp, 16
	cvtss2si ecx, xmm0 ; x Pos in rcx
	shufps xmm0, xmm0, 0b11010001
	cvtss2si r15d, xmm0 ; y Pos in r15, save for later
	
	mov r8d, dword [dwBallMoveRight]
	cmp r8d, 0
	je __MoveBallComparePositionLeft


	__MoveBallComparePositionRight:
	mov eax, dword [videomode]
	sub eax, 0x14
	cmp ecx, eax
	jl __MoveBallDoneCompareLeftRightPosition
		; To do add logic with paddles here
		__MoveBallSetToLeft:
		;movd xmm0, dword [fPaddleLeftPos] ; Gets y value
		;cvtss2si eax, xmm0 
		;add eax, 100 ; 1/2 size of y side of paddle.
		;cmp ecx, eax 
		;jg __RightPaddleOutOfBounds
		;sub eax, 200 ; size of full length paddle
		;jl __RightPaddleOutOfBounds


		mov dword [dwBallMoveRight], 0	
		movss xmm0, dword [fBallUpdateMultiplier]
		movss xmm1, dword [fNegTwoX]
		addps xmm0, xmm1
		movss dword [fBallUpdateMultiplier], xmm0

	jmp __MoveBallDoneCompareLeftRightPosition	
		
		__RightPaddleOutOfBounds:

		
		jmp __MoveBallDoneCompareLeftRightPosition 

	__MoveBallComparePositionLeft:
	cmp rcx, 0
	jg __MoveBallDoneCompareLeftRightPosition
		; To do add logic with paddles here
		__MoveBallSetToRight:	
		mov dword [dwBallMoveRight], 1
		movss xmm0, dword [fBallUpdateMultiplier]
		movss xmm1, dword [fPosTwoX]
		addps xmm0, xmm1
		movss dword [fBallUpdateMultiplier], xmm0
		


	; Now lets compare up and down.
	__MoveBallDoneCompareLeftRightPosition:
	mov r8d, dword [dwBallMoveUp]
	cmp r8d, 0
	je __MoveBallComparePositionDown

	__MoveBallComparePositionUp:
	cmp r15d, 0x01
	jg __MoveBallDoneCompareUpDownPosition

		__MoveBallSetToDown:
		mov dword [dwBallMoveUp], 0
		movss xmm0, dword [fBallUpdateMultiplier+4]
		movss xmm1, dword [fNegTwoY+4]
		addps xmm0, xmm1
		movss dword [fBallUpdateMultiplier+4], xmm0

	__MoveBallComparePositionDown:
	mov ecx, dword [videomode+sfVideoMode.Height]
	sub ecx, 0x14
	cmp r15d, ecx
	jl __MoveBallDoneCompareUpDownPosition

		__MoveBallSetToUp:
		mov dword [dwBallMoveUp], 1
		movss xmm0, dword [fBallUpdateMultiplier+4]
		movss xmm1, dword [fPosTwoY+4]
		addps xmm0, xmm1
		movss dword [fBallUpdateMultiplier+4], xmm0



	__MoveBallDoneCompareUpDownPosition:
	
	ret


__MoveBallLeft:

__MoveBallRight:




__MoveLeftPaddleUp:
	mov rdi, [lpshpPaddleLeft]
	mov rcx, qword [fMoveUp]
	movq xmm0, rcx
	sub rsp, 16
	call sfRectangleShape_move wrt ..plt
	add rsp, 16
	ret

__MoveLeftPaddleDown:
	mov rdi, [lpshpPaddleLeft]
	mov rcx, qword [fMoveDown]
	movq xmm0, rcx
	sub rsp, 16
	call sfRectangleShape_move wrt ..plt
	add rsp, 16
	ret


__MoveRightPaddleUp:
	mov rdi, [lpshpPaddleRight]
	mov rcx, qword [fMoveUp]
	movq xmm0, rcx
	sub rsp, 16
	call sfRectangleShape_move wrt ..plt
	add rsp, 16
	ret

__MoveRightPaddleDown:
	mov rdi, [lpshpPaddleRight]
	mov rcx, qword [fMoveDown]
	movq xmm0, rcx
	sub rsp, 16
	call sfRectangleShape_move wrt ..plt
	add rsp, 16
	ret


__HandleInput:






STRUC sfEvent
	.Type:		RESD	1
			RESD 	5 	; Largest event struct, since this is a union
	.Size:
ENDSTRUC

STRUC sfKeyEvent
	.Type:		RESD 	1
	.KeyCode:	RESD 	1
	.Alt:		RESD 	1
	.Control:	RESD 	1
	.System:	RESD 	1
	.Size:
ENDSTRUC


sfEvtKeyPressed 	equ	5

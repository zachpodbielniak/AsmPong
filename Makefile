CC = gcc
ASM = nasm

ASM_FLAGS = -Wall


all:	AsmPong

debug:	AsmPong_d

FILES = Main.o

FILES_D = Main_d.o


AsmPong: $(FILES)
	$(CC) -s -o bin/AsmPong bin/*.o -lpodnet -lcsfml-system -lcsfml-window -lcsfml-graphics
	rm bin/*.o



AsmPong_d: $(FILES_D)
	$(CC) -g -o bin/AsmPong_d bin/*_d.o -lpodnet -lcsfml-system -lcsfml-window -lcsfml-graphics 	
	rm bin/*.o





Main.o:
	$(ASM) -f elf64 $(ASM_FLAGS) -o bin/Main.o Source/Main.asm -I Source/





Main_d.o:
	$(ASM) -g -F dwarf -f elf64 $(ASM_FLAGS) -o bin/Main_d.o Source/Main.asm -I Source/

clean:
	rm bin/*
